package pluang.pritomdutta.codingassignment.core.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class AppGlideModule: AppGlideModule()