package pluang.pritomdutta.codingassignment.helper

import pluang.pritomdutta.network.model.Photo
import java.util.ArrayList

interface ClickListener {
    fun onClick(data: Photo,view: SquareImageView,position:Int,list: ArrayList<Photo>)
}