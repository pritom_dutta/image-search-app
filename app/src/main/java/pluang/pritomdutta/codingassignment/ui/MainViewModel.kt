package pluang.pritomdutta.codingassignment.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.filter
import androidx.paging.rxjava3.cachedIn
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import pluang.pritomdutta.codingassignment.utils.NetworkState
import pluang.pritomdutta.network.model.Photo
import pluang.pritomdutta.network.repository.FlickerRepository
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: FlickerRepository) : ViewModel() {
    private val _compositeDisposable by lazy { CompositeDisposable() }


    private val _loadingState by lazy { MutableLiveData<NetworkState>() }
    val loadingState: MutableLiveData<NetworkState>
        get() = _loadingState

    private val _errorState by lazy { MutableLiveData<NetworkState>() }

    val errorState: MutableLiveData<NetworkState>
        get() = _errorState

    private val _photoList by lazy { MutableLiveData<List<Photo>>() }
    val photoList: MutableLiveData<List<Photo>>
        get() = _photoList

//    fun getPhotoList(text: String, page: Int) {
//        _compositeDisposable.add(getData(text,page).subscribe {
//           // mAdapter.submitData(lifecycle, it)
//            Log.e("getPhotoList", "getPhotoList: "+it.toString())
//        })
//        val disposable = getData(text,page)
//            .doOnSubscribe {
//                _loadingState.postValue(NetworkState.Loading(isLoading = true))
//            }.doAfterTerminate {
//                _loadingState.postValue(NetworkState.Loading(isLoading = false))
//            }.subscribe({
//                _photoList.value = it
//            }, {
//                _errorState.value = NetworkState.Error(it)
//            })

//        _compositeDisposable.add(disposable)


//    }
//
//    }

    fun getData(text: String, page: Int): Flowable<PagingData<Photo>> {
        return repository
            .getPhotos(text, page)
            .map { pagingData ->
                pagingData.filter {
                    it.id != null
                }
            }
            .cachedIn(viewModelScope)
    }

    override fun onCleared() {
        super.onCleared()
        _compositeDisposable.dispose()
    }
}