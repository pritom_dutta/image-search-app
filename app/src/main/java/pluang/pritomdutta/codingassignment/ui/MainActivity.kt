package pluang.pritomdutta.codingassignment.ui

import android.app.ActivityOptions
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.SharedElementCallback
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.disposables.CompositeDisposable
import pluang.pritomdutta.codingassignment.R
import pluang.pritomdutta.codingassignment.adapters.ImagePagingAdapter
import pluang.pritomdutta.codingassignment.adapters.LoadingGridStateAdapter
import pluang.pritomdutta.codingassignment.databinding.ActivityMainBinding
import pluang.pritomdutta.codingassignment.helper.ClickListener
import pluang.pritomdutta.codingassignment.helper.SquareImageView
import pluang.pritomdutta.codingassignment.ui.viewimage.ViewImageActivity
import pluang.pritomdutta.codingassignment.utils.NetworkState
import pluang.pritomdutta.network.model.Photo


@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ClickListener {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding
//    private lateinit var mImageAdapter: ImageAdapter
    private lateinit var mAdapter: ImagePagingAdapter
    private val mDisposable = CompositeDisposable()

    companion object {
        var currentPosition = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt("currentPosition", 0)
            return
        }
        mAdapter = ImagePagingAdapter()

        binding.rvList.layoutManager = GridLayoutManager(applicationContext, 2)
       // mImageAdapter = ImageAdapter(applicationContext)
        binding.rvList.adapter = mAdapter

        binding.rvList.adapter = mAdapter.withLoadStateFooter(
            footer = LoadingGridStateAdapter()
        )
        mAdapter.addLoadStateListener { loadState ->
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error

            errorState?.let {
                AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(it.error.localizedMessage)
                    .setNegativeButton(R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .setPositiveButton(R.string.retry) { _, _ ->
                        mAdapter.retry()
                    }
                    .show()
            }
        }

        mAdapter.setListener(this)
        initNetworkStateObserver()
        initImgListObserver()
        prepareTransitions()
    }

    private fun initNetworkStateObserver() {
        viewModel.loadingState.observe(this, { networkState ->
            binding.progressBar.isVisible = (networkState as NetworkState.Loading).isLoading
        })

        viewModel.errorState.observe(this, { networkState ->
            Log.e("error", "errorState")
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu?.findItem(R.id.action_search)
        val searchView: SearchView = menuItem?.actionView as SearchView
        searchView.queryHint = "Type here to search"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("onQueryTextSubmit", "onQueryTextSubmit: "+query )
//                viewModel.

                mDisposable.add(viewModel.getData(query, 1).subscribe {
                    mAdapter.submitData(lifecycle, it)
                })
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                //adapter.filter.filter(newText)
                return false
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemMenu = item.itemId

        if (itemMenu == R.id.action_two) {
            binding.rvList.layoutManager = GridLayoutManager(applicationContext, 2)
        } else if (itemMenu == R.id.action_three) {
            binding.rvList.layoutManager = GridLayoutManager(applicationContext, 3)
        } else if (itemMenu == R.id.action_four) {
            binding.rvList.layoutManager = GridLayoutManager(applicationContext, 4)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initImgListObserver() {
        viewModel.photoList.observe(this, { imgList ->
            if (imgList.isNotEmpty()) {
                initAdapter(imgList)
            }
        })
    }

    private fun initAdapter(list: List<Photo>) {
       // mImageAdapter.setDataList(list)
    }

    override fun onClick(data: Photo, view: SquareImageView,position:Int,list: ArrayList<Photo>) {
        val transitionName = view.getTransitionName()
        currentPosition = position
        val intent = Intent(this, ViewImageActivity::class.java).apply {
            putExtra("imglist", data.imgUrl)
            putExtra("position", position)
            putParcelableArrayListExtra("list", mAdapter.snapshot().items as ArrayList<Photo>)
        }
        val activityOptions = ActivityOptions.makeSceneTransitionAnimation(
            this, view,
            "view_pager")
        startActivity(intent, activityOptions.toBundle())
    }

    override fun onResume() {
        super.onResume()
        scrollToPosition()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentPosition", currentPosition)
    }

    private fun scrollToPosition() {
        Log.e("currentPosition", "scrollToPosition: " +currentPosition)
        binding.rvList.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int,
                oldLeft: Int,
                oldTop: Int,
                oldRight: Int,
                oldBottom: Int
            ) {
                binding.rvList.removeOnLayoutChangeListener(this)
                val layoutManager: RecyclerView.LayoutManager = binding.rvList.layoutManager!!
                val viewAtPosition = layoutManager.findViewByPosition(currentPosition)
                // Scroll to position if the view for the current position is null (not currently part of
                // layout manager children), or it's not completely visible.
                if (viewAtPosition == null || layoutManager
                        .isViewPartiallyVisible(viewAtPosition, false, true)
                ) {
                    binding.rvList.post(Runnable { layoutManager.scrollToPosition(currentPosition) })
                }
            }
        })
    }

    private fun prepareTransitions() {
        // A similar mapping is set at the ImagePagerFragment with a setEnterSharedElementCallback.
        setExitSharedElementCallback(
            object : SharedElementCallback() {
                override fun onMapSharedElements(
                    names: List<String>,
                    sharedElements: MutableMap<String, View>
                ) {
                    // Locate the ViewHolder for the clicked position.
                    val selectedViewHolder: RecyclerView.ViewHolder = binding.rvList
                        .findViewHolderForAdapterPosition(currentPosition) ?: return

                    // Map the first shared element name to the child ImageView.
                    sharedElements[names[0]] =
                        selectedViewHolder.itemView.findViewById(R.id.img_squareImageView)
                }
            })
    }
}