package pluang.pritomdutta.codingassignment.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import pluang.pritomdutta.codingassignment.R
import pluang.pritomdutta.codingassignment.core.glide.GlideApp
import pluang.pritomdutta.codingassignment.databinding.FragmentImageBinding

class ImageFragment : Fragment() {
    private lateinit var binding: FragmentImageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_image, container, false)
        val arguments = arguments
        val imgurl = arguments!!.getString("drawableRes")
        binding.imgData.apply {
            if (!imgurl.isNullOrEmpty()) {
                GlideApp.with(this)
                    .load(imgurl)
                    .placeholder(R.drawable.ic_image)
                    .error(R.drawable.ic_launcher_foreground)
                    .into(this)
            } else {
                GlideApp.with(this)
                    .load(R.drawable.ic_image)
                    .into(this)
            }
        }
        return binding.root
    }

    companion object {
        fun newInstance(drawableRes: String) =
            ImageFragment().apply {
                arguments = Bundle().apply {
                    putString("drawableRes", drawableRes)
                }
            }
    }
}