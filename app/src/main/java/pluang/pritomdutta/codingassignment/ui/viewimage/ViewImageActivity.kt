package pluang.pritomdutta.codingassignment.ui.viewimage

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener
import pluang.pritomdutta.codingassignment.R
import pluang.pritomdutta.codingassignment.adapters.ImagePagerAdapter
import pluang.pritomdutta.codingassignment.databinding.ActivityViewImageBinding
import pluang.pritomdutta.codingassignment.ui.MainActivity
import pluang.pritomdutta.network.model.Photo

class ViewImageActivity : AppCompatActivity() {



    private lateinit var binding: ActivityViewImageBinding
    private lateinit var list: List<Photo>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_image)

        val imgurl: String = intent.getStringExtra("imgurl").toString()
        list = intent.getParcelableArrayListExtra<Photo>("list")!!
        val pos = intent.getIntExtra("position",0)

        binding.viewPager.adapter = ImagePagerAdapter(supportFragmentManager,list)
        binding.viewPager.setCurrentItem(MainActivity.currentPosition)
        binding.viewPager.addOnPageChangeListener(object : SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                MainActivity.currentPosition = position
            }
        })
        Log.e("Loag", "onCreate: "+list.size )

    }

}