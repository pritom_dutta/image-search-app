package pluang.pritomdutta.codingassignment.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import pluang.pritomdutta.codingassignment.R
import pluang.pritomdutta.codingassignment.core.glide.GlideApp

@BindingAdapter("image_url")
fun ImageView.loadImage(url: String?) {
    if (!url.isNullOrEmpty()) {
        GlideApp.with(this)
            .load(url)
            .placeholder(R.drawable.ic_image)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_launcher_foreground)
            .into(this)
    } else {
        GlideApp.with(this)
            .load(R.drawable.ic_image)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)
    }
}