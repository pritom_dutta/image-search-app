package pluang.pritomdutta.codingassignment.utils

sealed class NetworkState {
    data class Loading(val isLoading: Boolean = false) : NetworkState()
    data class Error(var exception: Throwable) : NetworkState()
}