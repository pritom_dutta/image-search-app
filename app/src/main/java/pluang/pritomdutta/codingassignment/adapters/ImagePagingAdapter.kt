package pluang.pritomdutta.codingassignment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pluang.pritomdutta.codingassignment.databinding.ImageItemBinding
import pluang.pritomdutta.codingassignment.helper.ClickListener
import pluang.pritomdutta.network.model.Photo

class ImagePagingAdapter: PagingDataAdapter<Photo, ImagePagingAdapter.ImgRXViewHolder> (COMPARATOR){

    lateinit var clickListener: ClickListener

    inner class ImgRXViewHolder(private val binding: ImageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(mPhoto: Photo) {
            binding.photo = mPhoto
            itemView.setOnClickListener(View.OnClickListener {
                if (clickListener != null) {
                    clickListener.onClick(
                        mPhoto,
                        binding.imgSquareImageView,
                        adapterPosition,
                        ArrayList<Photo>()
                    )
                }
            })
            binding.executePendingBindings()
        }
    }

    internal fun setListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun onBindViewHolder(holder: ImgRXViewHolder, position: Int) {
        getItem(position)?.let {
            it.imgUrl = "http://farm"+it.farm+".static.flickr.com/"+it.server+"/"+it.id+"_"+it.secret+".jpg"
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImgRXViewHolder {
        return ImgRXViewHolder(
            ImageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean {
                return oldItem == newItem
            }
        }
    }
}