package pluang.pritomdutta.codingassignment.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import pluang.pritomdutta.codingassignment.ui.fragment.ImageFragment
import pluang.pritomdutta.network.model.Photo

class ImagePagerAdapter(fm: FragmentManager,var list: List<Photo>) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Fragment {
        val imgUrl = "http://farm"+list.get(position).farm+".static.flickr.com/"+list.get(position).server+"/"+list.get(position).id+"_"+list.get(position).secret+".jpg"
        return ImageFragment.newInstance(imgUrl)
    }
}