package pluang.pritomdutta.codingassignment.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import pluang.pritomdutta.codingassignment.testUtils.TestUtils.getPhotoTestData
import pluang.pritomdutta.codingassignment.utils.NetworkState
import pluang.pritomdutta.network.model.Photo
import pluang.pritomdutta.network.repository.FlickerRepository

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest{
    companion object{
        const val INDEX_0_CAR_NAME = "7457595@N05"
        const val INDEX_0_LICENCE_PLATE_NUMBER = "2dbc237508"

        const val INDEX_9_CAR_NAME = "24406544@N00"
        const val INDEX_9_LICENCE_PLATE_NUMBER = "353f0468da"
    }
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var mockFlickerRepository: FlickerRepository


    @Mock
    private lateinit var mockNetworkStateObserver: Observer<NetworkState>

    private val mockCarInfoListObserver by lazy { TestObserver<List<Photo>>() }

    private val fakeNetworkObserver by lazy { TestObserver<NetworkState>() }

    private lateinit var sutViewModel: MainViewModel


    @Before
    fun setup(){
        sutViewModel = MainViewModel(mockFlickerRepository)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler { }
    }

    @Test
    fun `get car list and verify network state interaction`(){
        //success()

        sutViewModel.loadingState.observeForever(mockNetworkStateObserver)
        sutViewModel.getData("Cat",1)

        //network state checking in order
        val inOrder = Mockito.inOrder(mockNetworkStateObserver)

        //loading
        inOrder.verify(mockNetworkStateObserver).onChanged(NetworkState.Loading(isLoading = true))
        //loaded
        inOrder.verify(mockNetworkStateObserver).onChanged(NetworkState.Loading(isLoading = false))

        inOrder.verifyNoMoreInteractions()
    }

    @Test
    fun `get car list and correct card data returned`(){
        //success()

        sutViewModel.photoList.observeForever(mockCarInfoListObserver)
        sutViewModel.getData("man",1)

        Thread.sleep(100)
        Assertions.assertThat(mockCarInfoListObserver.value?.size).isEqualTo(100)

        assertThat(mockCarInfoListObserver.value?.get(0)?.owner).isEqualTo(INDEX_0_CAR_NAME)
        assertThat(mockCarInfoListObserver.value?.get(0)?.secret).isEqualTo(INDEX_0_LICENCE_PLATE_NUMBER)

        assertThat(mockCarInfoListObserver.value?.get(9)?.owner).isEqualTo(INDEX_9_CAR_NAME)
        assertThat(mockCarInfoListObserver.value?.get(9)?.secret).isEqualTo(INDEX_9_LICENCE_PLATE_NUMBER)
    }

//    @Test
//    fun `get car list and verify failure network state interaction`(){
//        //failure()
//
//        sutViewModel.errorState.observeForever(fakeNetworkObserver)
//        sutViewModel.getData("man",1)
//
//        Thread.sleep(100)
//        assertThat(fakeNetworkObserver.value).isInstanceOf(NetworkState.Error::class.java)
//    }

//    private fun success() {
//        val testData = getPhotoTestData("photoList.json")
//        Mockito.`when`(mockFlickerRepository.getPhotos("man",1)).thenReturn(Single.create { emitter ->
//            emitter.onSuccess(testData)
//        })
//    }


//    private fun failure() {
//        Mockito.`when`(mockCarInfoRepository.getCar()).thenReturn(Single.create { emitter ->
//            emitter.onError(RequestException("No car found"))
//        })
//    }

    private class TestObserver<T> : Observer<T> {
        var value: T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }
}