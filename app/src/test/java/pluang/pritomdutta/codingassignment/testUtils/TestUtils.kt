package pluang.pritomdutta.codingassignment.testUtils

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import pluang.pritomdutta.network.model.Photo
import java.io.IOException
import java.lang.reflect.Type

object TestUtils {
    @Throws(IOException::class)
    private fun readFileToString(contextClass: Class<*>, fileName: String): String {
        contextClass.getResourceAsStream(fileName)
            ?.bufferedReader().use {
                val jsonString = it?.readText() ?: ""
                it?.close()
                return jsonString
            }
    }

    fun getPhotoTestData(fileName: String): List<Photo> {
        val moshi = Moshi.Builder()
            .build()
        val listData: Type = Types.newParameterizedType(
            List::class.java,
            Photo::class.java
        )
        val adapter: JsonAdapter<List<Photo>> = moshi.adapter(listData)
        val jsonString = readFileToString(TestUtils::class.java, "/$fileName")
        return adapter.fromJson(jsonString)!!
    }
}