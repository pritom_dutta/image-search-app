package pluang.pritomdutta.network.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pluang.pritomdutta.network.db.dao.FlickerDao
import pluang.pritomdutta.network.model.Photo


@Database(
    entities = [Photo::class],
    version = 1,
    exportSchema = false
)
abstract class FlickerDB : RoomDatabase() {
    abstract fun flickerDao(): FlickerDao

}