package pluang.pritomdutta.network.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import pluang.pritomdutta.network.model.Photo

@Dao
interface FlickerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePhotoList(photos: List<Photo>)

    @Query("DELETE FROM photo_data")
    fun deleteAllPhotoList();

    @Query("SELECT * FROM photo_data")
    fun getPhoto(): List<Photo>
}