package pluang.pritomdutta.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


data class Photos(
    @Json(name = "page")
    val page: Int = 0,
    @Json(name = "pages")
    val pages: Int = 0,
    @Json(name = "perpage")
    val perpage: Int = 0,
    @Json(name = "photo")
    val photo: List<Photo>,
    @Json(name = "total")
    val total: Int = 0
)