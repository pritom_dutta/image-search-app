package pluang.pritomdutta.network.model


import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = "photo_data")
data class Photo(

    @PrimaryKey
    val uid: Int = 0,
    @Json(name = "farm")
    val farm: Int = 0,
    @Json(name = "id")
    val id: String? = null,
    @Json(name = "isfamily")
    val isfamily: Int = 0,
    @Json(name = "isfriend")
    val isfriend: Int = 0,
    @Json(name = "ispublic")
    val ispublic: Int = 0,
    @Json(name = "owner")
    val owner: String? = null,
    @Json(name = "secret")
    val secret: String? = null,
    @Json(name = "server")
    val server: String? = null,
    @Json(name = "title")
    val title: String? = null
): Parcelable{
    @Transient
    var position = -1
    @Transient
    var imgUrl = ""

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(uid)
        parcel.writeInt(farm)
        parcel.writeString(id)
        parcel.writeInt(isfamily)
        parcel.writeInt(isfriend)
        parcel.writeInt(ispublic)
        parcel.writeString(owner)
        parcel.writeString(secret)
        parcel.writeString(server)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Photo> {
        override fun createFromParcel(parcel: Parcel): Photo {
            return Photo(parcel)
        }

        override fun newArray(size: Int): Array<Photo?> {
            return arrayOfNulls(size)
        }
    }

}
