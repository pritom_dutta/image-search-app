package pluang.pritomdutta.network.model

import io.reactivex.rxjava3.core.Single

class PhotoMapper {

    fun transform(response: PhotoResponse): Photos {
        return with(response) {
            Photos(0,0,0,photos.photo)
        }
    }
}