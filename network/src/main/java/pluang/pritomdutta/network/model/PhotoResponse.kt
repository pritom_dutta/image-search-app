package pluang.pritomdutta.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


data class PhotoResponse(
    @Json(name = "photos")
    val photos: Photos,
    @Json(name = "stat")
    val stat: String? = null
)