package pluang.pritomdutta.network.di

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pluang.pritomdutta.network.db.FlickerDB
import pluang.pritomdutta.network.db.dao.FlickerDao
import pluang.pritomdutta.network.repository.FlickerRepository
import pluang.pritomdutta.network.repository.FlickerRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun provideRepository(api: FlickerRepositoryImpl): FlickerRepository
}