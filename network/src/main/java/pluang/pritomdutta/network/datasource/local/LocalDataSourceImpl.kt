package pluang.pritomdutta.network.datasource.local

import pluang.pritomdutta.network.db.dao.FlickerDao
import pluang.pritomdutta.network.model.Photo
import javax.inject.Inject


class LocalDataSourceImpl @Inject constructor (private val mFlickerDao: FlickerDao) : FlickerLocalDataSource {

    override fun getPhotoFromDB(): List<Photo> = mFlickerDao.getPhoto()

    override fun savePhotoFromDB(photos: List<Photo>) {
        mFlickerDao.savePhotoList(photos)
    }

    override fun clearAll() {
        mFlickerDao.deleteAllPhotoList()
    }
}