package pluang.pritomdutta.network.datasource

import io.reactivex.rxjava3.core.Single
import pluang.pritomdutta.network.model.PhotoResponse
import pluang.pritomdutta.network.onResponse
import javax.inject.Inject

class FlickerApi @Inject constructor(private val service: FlickerService) {
    fun getPhotos(text: String, page: Int): Single<PhotoResponse> {
        return service.getPhoto(text, page)
            .onResponse()
    }
}