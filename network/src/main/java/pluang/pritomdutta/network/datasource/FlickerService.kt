package pluang.pritomdutta.network.datasource

import io.reactivex.rxjava3.core.Single
import pluang.pritomdutta.network.model.PhotoResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickerService {
    @GET("?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&%20format=json&nojsoncallback=1&safe_search=1")
    fun getPhoto(@Query("text") text:String, @Query("page") page: Int): Single<Response<PhotoResponse>>
}