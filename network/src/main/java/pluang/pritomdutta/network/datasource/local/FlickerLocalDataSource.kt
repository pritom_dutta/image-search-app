package pluang.pritomdutta.network.datasource.local

import pluang.pritomdutta.network.model.Photo

interface FlickerLocalDataSource {
    fun getPhotoFromDB(): List<Photo>
    fun savePhotoFromDB(photos: List<Photo>)
    fun clearAll()
}