package pluang.pritomdutta.network.repository


import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pluang.pritomdutta.network.datasource.FlickerApi
import pluang.pritomdutta.network.model.Photo
import pluang.pritomdutta.network.model.PhotoMapper
import pluang.pritomdutta.network.model.Photos
import javax.inject.Inject

class FlickerPagingSource constructor(
    private val api: FlickerApi,
    private val query: String
) : RxPagingSource<Int, Photo>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Photo>> {
        val position = params.key ?: 1
        val mapper = PhotoMapper()
        return api.getPhotos(query, position)
            .subscribeOn(Schedulers.io())
            .map { mapper.transform(it) }
            .map { toLoadResult(it, position) }
            .onErrorReturn { LoadResult.Error(it) }
    }

    private fun toLoadResult(data: Photos, position: Int): LoadResult<Int, Photo> {
        return LoadResult.Page(
            data = data.photo,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == data.total) null else position + 1
        )
    }

    override fun getRefreshKey(state: PagingState<Int, Photo>): Int? {
        return null
    }
}