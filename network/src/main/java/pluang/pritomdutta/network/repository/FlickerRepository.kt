package pluang.pritomdutta.network.repository


import androidx.paging.PagingData
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import pluang.pritomdutta.network.model.Photo

interface FlickerRepository {
    fun getPhotos(text:String,page: Int): Flowable<PagingData<Photo>>
}