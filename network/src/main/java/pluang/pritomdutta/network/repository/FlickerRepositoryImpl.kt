package pluang.pritomdutta.network.repository


import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import androidx.paging.rxjava3.flowable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import pluang.pritomdutta.network.datasource.FlickerApi
import pluang.pritomdutta.network.model.Photo
import pluang.pritomdutta.network.onException
import javax.inject.Inject

class FlickerRepositoryImpl @Inject constructor(
    private val api: FlickerApi
    ) : FlickerRepository {
    override fun getPhotos(text: String, page: Int): Flowable<PagingData<Photo>> {
        return Pager(
            config = PagingConfig(
                pageSize = 100,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { FlickerPagingSource(api, text) }
        ).flowable
    }
    }
