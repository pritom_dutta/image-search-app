package pluang.pritomdutta.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Error(
    val message: String? = null
)