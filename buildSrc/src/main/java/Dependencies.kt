//run this to get updated dependencies ./gradlew dependencyUpdates -DoutputDir=VersionReport
object Versions {
    const val gradlePlugin = "4.2.2"

    const val kotlin = "1.5.21"
    const val coreKtx = "1.6.0"

    const val appCompat = "1.3.1"
    const val constraintLayout = "2.1.0"
    const val fragmentKtx = "1.3.6"
    const val navigation = "2.3.0-alpha01"
    const val lifecycleRuntimeKTX = "2.3.1"

    const val material = "1.4.0"

    const val hilt = "2.38.1"
    const val hiltNavigation = "1.0.0"

    const val rxAndroid = "3.0.0"
    const val rxJava = "3.1.0"

    const val retrofit = "2.9.0"
    const val gson = "2.8.9"
    const val loggingInterceptor = "3.1.0"
    const val moshi = "1.12.0"
    const val stetho = "1.5.1"

    const val sdpssp = "1.0.6"

    const val glide = "4.12.0"

    const val jUnit = "4.13.2"
    const val extJunit = "1.1.3"
    const val espressoCore = "3.2.0"
    const val mockWebServer = "4.9.1"
    const val mockito = "3.8.0"
    const val assertj = "3.20.2"
    const val androidxTestRunner = "1.4.0"
    const val allOpen = "1.5.21"
    const val androidArchCore = "2.1.0"
    const val androidXTestRule = "1.4.0"
    const val navigationTesting = "2.3.0-alpha01"
    const val espressoCContribAndIntent = "3.2.0"
    const val espressoIdlingAndConcurrent = "3.2.0"

    const val paging_version = "3.1.0"
}


/**
 * To define plugins
 */
object BuildPlugins {
    val androidBuildTools = "com.android.tools.build:gradle:${Versions.gradlePlugin}"
    val kotlinPlugins = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
    val hiltPlugin = "com.google.dagger:hilt-android-gradle-plugin:${Versions.hilt}"
    val navigationSafeArg = "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
    val allOpenPlugin = "org.jetbrains.kotlin:kotlin-allopen:${Versions.allOpen}"
}

/**
 * To define dependencies
 */
object KotlinDependencies {
    val kotlinStd = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
}

object AndroidXSupportDependencies {
    val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    val lifecycleRuntimeKTX = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleRuntimeKTX}"
}

object MaterialDesignDependencies {
    val materialDesign = "com.google.android.material:material:${Versions.material}"
}

object TestingDependencies {
    val junit = "junit:junit:${Versions.jUnit}"
    val androidExtJunit = "androidx.test.ext:junit:${Versions.extJunit}"
    val androidTestRunner = "androidx.test:runner:${Versions.androidxTestRunner}"
    val androidTestRule = "androidx.test:rules:${Versions.androidXTestRule}"
    val androidEspressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
    val mockWebServer = "com.squareup.okhttp3:mockwebserver:${Versions.mockWebServer}"
    val mockito = "org.mockito:mockito-core:${Versions.mockito}"
    val mockitoAndroid = "org.mockito:mockito-android:${Versions.mockito}"
    val assertj = "org.assertj:assertj-core:${Versions.assertj}"
    val androidArchCoreTesting = "androidx.arch.core:core-testing:${Versions.androidArchCore}"
    val fragmentTesting = "androidx.fragment:fragment-testing:${Versions.fragmentKtx}"
    val navigationTesting = "androidx.navigation:navigation-testing:${Versions.navigationTesting}"
    val espressoContrib = "androidx.test.espresso:espresso-contrib:${Versions.espressoCContribAndIntent}"
    val espressoIntent = "androidx.test.espresso:espresso-intents:${Versions.espressoCContribAndIntent}"
    val espressoConcurrent = "androidx.test.espresso.idling:idling-concurrent:${Versions.espressoIdlingAndConcurrent}"
    val espressoIdling = "androidx.test.espresso:espresso-idling-resource:${Versions.espressoIdlingAndConcurrent}"

}

object Libraries {
    val hilt = "com.google.dagger:hilt-android:${Versions.hilt}"
    val hiltAnnotationProcessor = "com.google.dagger:hilt-android-compiler:${Versions.hilt}"
    val hiltNavigation = "androidx.hilt:hilt-navigation-fragment:${Versions.hiltNavigation}"

    //For instrumentation tests
    val hiltInstrumentation = "com.google.dagger:hilt-android-testing:${Versions.hilt}"
    val hiltInsAnnotationProcessor = "com.google.dagger:hilt-android-testing:${Versions.hilt}"

    //For local unit tests
    val hiltUnitTest = "com.google.dagger:hilt-android-testing:${Versions.hilt}"
    val hiltUnitTestAnnotationProcessor = "com.google.dagger:hilt-compiler:${Versions.hilt}"

    //rxjava and rxAndroid
    val rxAndroid = "io.reactivex.rxjava3:rxandroid:${Versions.rxAndroid}"

    //logging interceptor
    val loggingInterceptor = "com.github.ihsanbal:LoggingInterceptor:${Versions.loggingInterceptor}"

    //network interceptor
    val stetho = "com.facebook.stetho:stetho:${Versions.stetho}"
    val stethoOkhttp = "com.facebook.stetho:stetho-okhttp3:${Versions.stetho}"
    val stethoJSRhino = "com.facebook.stetho:stetho-js-rhino:${Versions.stetho}"

    //moshi
    val moshi = "com.squareup.moshi:moshi:${Versions.moshi}"
    val moshiKotlinCodeGen = "com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}"
    val moshiKotlin = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"

    //retrofit
    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitMoshiConverter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
    val gsonCon =  "com.google.code.gson:gson:${Versions.gson}"
    val gsonConverter =  "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    val retrofitRxAdapter = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit}"


    val sdp = "com.intuit.sdp:sdp-android:${Versions.sdpssp}"
    val ssp = "com.intuit.ssp:ssp-android:${Versions.sdpssp}"

    val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
    val glideKapt = "com.github.bumptech.glide:compiler:${Versions.glide}"

    val paging = "androidx.paging:paging-runtime-ktx:${Versions.paging_version}"
    val rxpaging = "androidx.paging:paging-rxjava3:${Versions.paging_version}"

}