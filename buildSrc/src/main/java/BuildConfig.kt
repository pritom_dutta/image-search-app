object BuildConfig {
    const val applicationID = "pluang.pritomdutta.codingassignment"
    const val compileSdkVersion = 31
    const val buildToolsVersion = "30.0.3"
    const val minSdkVersion = 22
    const val targetSdkVersion = 30
    const val versionCode = 1
    const val versionName = "1.0"

    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}